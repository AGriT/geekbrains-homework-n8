﻿#include <iostream>
#include <random>

enum class CellState 
{
	Empty, 
	X, 
	O
};

enum class Sign 
{
	X, 
	O
};

struct GameField 
{
	CellState* m_ptrCells;
	int m_nSize;
	int m_nSideSize;
	int m_nEmptyCells;
};

struct GameData 
{
	GameField m_stGF_field;
	Sign m_ePlayerSign;
	Sign m_eBotSign;
};

struct TurnOutcome 
{
	bool isOver : 1;
	Sign victor : 1;
	bool isDraw : 1;
};

bool isValidCell(int nSideSize, size_t row, size_t column)
{
	return row >= 0 && row < (size_t)nSideSize && column >= 0 && column < (size_t)nSideSize;
}

void vFillGameData(GameData& stData, Sign eSignPlayerSign, int nGameFieldSize)
{
	stData.m_ePlayerSign = eSignPlayerSign;
	stData.m_eBotSign = (eSignPlayerSign == Sign::X) ? Sign::O : Sign::X;

	stData.m_stGF_field.m_nSize = nGameFieldSize * nGameFieldSize;
	stData.m_stGF_field.m_ptrCells = new CellState[stData.m_stGF_field.m_nSize]{};
	stData.m_stGF_field.m_nEmptyCells = stData.m_stGF_field.m_nSize;
	stData.m_stGF_field.m_nSideSize = nGameFieldSize;
}

CellState getCell(const GameField& field, size_t row, size_t column) 
{
	if (!isValidCell(field.m_nSideSize, row, column)) 
		return CellState::Empty;

	return field.m_ptrCells[column * field.m_nSideSize + row];
}

bool isCellEmpty(const GameField& field, size_t row, size_t column) 
{
	return getCell(field, row, column) == CellState::Empty;
}

void putSign(GameField& field, Sign sign, size_t row, size_t column) 
{
	if (isValidCell(field.m_nSideSize, row, column))
	{
		field.m_ptrCells[column * field.m_nSideSize + row] = (sign == Sign::X) ? CellState::X : CellState::O;
		field.m_nEmptyCells--;
	}
}

bool askQuestion(char positive, char negative) 
{
	char sign{};
	std::cin >> sign;
	sign = tolower(sign);

	while (sign != positive && sign != negative)
	{
		std::cout << "Wrong input: received '" << sign << "', should be one of " << positive << " or " << negative << "\n";
		std::cin >> sign;
		sign = tolower(sign);
	}

	return sign == positive;
}

int nAskSize()
{
	uint32_t nSize = 0;
	std::cout << "Choose size of GameField\n";
	std::cin >> nSize;

	while (nSize <= 2)
	{
		std::cout << "Wrong input: received '" << nSize << "', should be bigger than 2\n";
		std::cin >> nSize;
	}

	std::cout << nSize << " x " << nSize <<" gamefield would be created\n";
	return nSize;
}

Sign queryPlayerSign() 
{
	std::cout << "Please input wheter you're X or O: ";
	bool isX = askQuestion('x', 'o');

	return isX ? Sign::X : Sign::O;
}

TurnOutcome checkTurnOutcome(const GameField & field);

void processAiTurn(GameData& stGameData)
{
	size_t* empty_cells = new size_t[stGameData.m_stGF_field.m_nEmptyCells];
	const size_t kInvalidCellIdx = 42;
	std::fill_n(empty_cells, stGameData.m_stGF_field.m_nEmptyCells, kInvalidCellIdx);
	size_t last_empty_cell_idx = 0;

	for (size_t i = 0; i < stGameData.m_stGF_field.m_nSize; i++)
	{
		if (stGameData.m_stGF_field.m_ptrCells[i] == CellState::Empty) 
		{
			empty_cells[last_empty_cell_idx] = i;
			last_empty_cell_idx++;
		}
	}

	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_int_distribution<size_t> dist(0, last_empty_cell_idx - 1);

	for (size_t i = 0; i < last_empty_cell_idx; i++) 
	{
		size_t target = empty_cells[i];
		stGameData.m_stGF_field.m_ptrCells[target] = stGameData.m_eBotSign == Sign::X ? CellState::X : CellState::O;
		auto outcome = checkTurnOutcome(stGameData.m_stGF_field);

		if (outcome.isOver) 
		{
			stGameData.m_stGF_field.m_nEmptyCells--;
			return;
		}

		stGameData.m_stGF_field.m_ptrCells[target] = CellState::Empty;

		auto opposite_sign = stGameData.m_ePlayerSign;
		stGameData.m_stGF_field.m_ptrCells[target] = opposite_sign == Sign::X ? CellState::X : CellState::O;
		outcome = checkTurnOutcome(stGameData.m_stGF_field);

		if (outcome.isOver) 
		{
			stGameData.m_stGF_field.m_ptrCells[target] = stGameData.m_eBotSign == Sign::X ? CellState::X : CellState::O;
			stGameData.m_stGF_field.m_nEmptyCells--;
			return;
		}

		stGameData.m_stGF_field.m_ptrCells[target] = CellState::Empty;
	}

	size_t target = empty_cells[dist(mt)];
	putSign(stGameData.m_stGF_field, stGameData.m_eBotSign, target % stGameData.m_stGF_field.m_nSideSize, target / stGameData.m_stGF_field.m_nSideSize);
}

void processPlayerTurn(GameData & stGameData) 
{
	std::cout << "Enter row and column: ";

	int row, column;
	std::cin >> row >> column;

	while (!isValidCell(stGameData.m_stGF_field.m_nSideSize, row - 1, column - 1) || !isCellEmpty(stGameData.m_stGF_field, row - 1, column - 1)) 
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Wrong input, please enter row and column of an empty cell: ";
		std::cin >> row >> column;
	}

	putSign(stGameData.m_stGF_field, stGameData.m_ePlayerSign, row - 1, column - 1);
}

void printField(const GameField & field);

bool isDraw(const GameField & field) 
{
	if (field.m_nEmptyCells != 0)
		return false;

	return true;
}

bool checkLine(const GameField & field, size_t start_row, size_t start_column, int delta_row, int delta_column) 
{
	size_t current_row = start_row;
	size_t current_column = start_column;
	auto first = getCell(field, start_row, start_column);
	if (first == CellState::Empty) 
		return false;

	while (isValidCell(field.m_nSideSize, current_row, current_column)) 
	{
		if (getCell(field, current_row, current_column) != first) 
			return false;

		current_row += delta_row;
		current_column += delta_column;
	}
	return true;
}

#define CHECK_LINE(start_row, start_column, delta_row, delta_column) \
        if (checkLine(field, start_row, start_column, delta_row, delta_column)) \
		{ \
            outcome.isOver = true; \
            outcome.victor = getCell(field, start_row, start_column) == CellState::X ? Sign::X : Sign::O; \
            return outcome; \
        }


TurnOutcome checkTurnOutcome(const GameField & field) 
{
	TurnOutcome outcome{};

	for (size_t row = 0; row < field.m_nSideSize; row++) 
		CHECK_LINE(row, 0, 0, 1)
	
	for (size_t column = 0; column < field.m_nSideSize; column++) 
		CHECK_LINE(0, column, 1, 0)
	
	CHECK_LINE(0, 0, 1, 1)

	CHECK_LINE(0, field.m_nSideSize, 1, -1)

	if (isDraw(field))
	{
		outcome.isDraw = true;
		outcome.isOver = true;
	}
	
	return outcome;
}

#undef CHECK_LINE

TurnOutcome runGameLoop(Sign player_sign, int nGameFieldSize) 
{
	GameData game_data{};
	TurnOutcome outcome{};

	vFillGameData(game_data, player_sign, nGameFieldSize);

	while (true) 
	{
		if (game_data.m_ePlayerSign == Sign::X) 
			processPlayerTurn(game_data);
		else 
			processAiTurn(game_data);
		

		printField(game_data.m_stGF_field);
		outcome = checkTurnOutcome(game_data.m_stGF_field);

		if (outcome.isOver)
			return outcome;

		if (game_data.m_ePlayerSign == Sign::O)
			processPlayerTurn(game_data);
		else 
			processAiTurn(game_data);

		printField(game_data.m_stGF_field);
		outcome = checkTurnOutcome(game_data.m_stGF_field);

		if (outcome.isOver)
			return outcome;
	}
}

void printField(const GameField & field) 
{
	system("cls");

	std::cout << "=========\n";
	for (size_t row = 0; row < field.m_nSideSize; row++) 
	{
		std::cout << "||";
		for (size_t column = 0; column < field.m_nSideSize; column++) 
		{
			switch (getCell(field, row, column))
			{
			case CellState::Empty:
				std::cout << " ";
				break;
			case CellState::X:
				std::cout << "X";
				break;
			case CellState::O:
				std::cout << "O";
				break;
			}

			std::cout << "|";
		}

		std::cout << "|\n";
	}

	std::cout << "=========\n";
}

bool queryPlayAgain() 
{
	std::cout << "Want to play again? [y or n]: ";
	return askQuestion('y', 'n');
}

void printGameOutcome(const TurnOutcome & outcome, Sign player_sign) 
{
	if (outcome.isDraw) 
		std::cout << "It's a draw.\n";
	else if (outcome.victor == player_sign) 
		std::cout << "Congrats! You're winner.\n";
	else 
		std::cout << "No luck this time.\n";
}

int main() 
{
	int nSize = nAskSize();

	Sign player_sign = queryPlayerSign();

	bool shouldExit = false;

	while (!shouldExit)
	{
		auto outcome = runGameLoop(player_sign, nSize);
		printGameOutcome(outcome, player_sign);
		shouldExit = !queryPlayAgain();
	}

	return 0;
}